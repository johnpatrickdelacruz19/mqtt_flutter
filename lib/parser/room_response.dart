import 'package:cass_rnd/model/room.dart';

class RoomsResponse {
  RoomsResponse(
    this.code,
    this.message,
    this.data,
  );

  RoomsResponse.fromJson(Map<String, dynamic> json)
      : data = (json["data"] as List).map((i) => Room.fromJson(i)).toList(),
        code = json["code"],
        message = json["message"];

  final List<Room> data;
  final int code;
  final String message;
}
