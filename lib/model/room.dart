class Room {
  Room(
    this.roomId,
    this.name,
    this.messageCount,
  );

  Room.fromJson(Map<String, dynamic> json)
      : roomId = json["room_id"],
        name = json["name"],
        messageCount = json["message_count"];

  final String roomId;
  final String name;
  final num messageCount;
}
