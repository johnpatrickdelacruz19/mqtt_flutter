import 'package:cass_rnd/bloc/chat/chat_bloc.dart';
import 'package:cass_rnd/bloc/conversation/conversation_bloc.dart';
import 'package:cass_rnd/bloc/conversation/conversation_event.dart';
import 'package:cass_rnd/bloc/notification/notification_bloc.dart';
import 'package:cass_rnd/bloc/notification/notification_event.dart';
import 'package:cass_rnd/bloc/register/register_bloc.dart';
import 'package:cass_rnd/bloc/splash/splash_bloc.dart';
import 'package:cass_rnd/bloc/splash/splash_event.dart';
import 'package:cass_rnd/locator.dart';
import 'package:cass_rnd/views/pages/chat_page.dart';
import 'package:cass_rnd/views/pages/conversation_page.dart';
import 'package:cass_rnd/views/pages/notification_page.dart';
import 'package:cass_rnd/views/pages/register_page.dart';
import 'package:cass_rnd/views/pages/splash_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class RouterManager {
  static Route generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case Routes.splash:
        return MaterialPageRoute(
          builder: (_) => MultiBlocProvider(
            providers: [
              BlocProvider(
                lazy: false,
                create: (context) =>
                    serviceLocator<SplashBloc>()..add(MaintenanceEvent()),
              ),
              BlocProvider(
                lazy: false,
                create: (context) =>
                    serviceLocator<NotificationBloc>()..add(NotificationLoad()),
              ),
            ],
            child: const SplashPage(),
          ),
        );

      case Routes.register:
        return MaterialPageRoute(
          builder: (_) => BlocProvider(
            create: (context) => serviceLocator<RegisterBloc>(),
            child: const RegisterPage(),
          ),
        );

      case Routes.chat:
        dynamic data = settings.arguments;

        return MaterialPageRoute(
          builder: (_) => BlocProvider(
            create: (context) => serviceLocator<ChatBloc>(),
            child: ChatPage(roomId: data[0], name: data[1]),
          ),
        );

      case Routes.conversation:
        return MaterialPageRoute(
          builder: (_) => BlocProvider(
            create: (context) => serviceLocator<ConversationBloc>()
              ..add(ConversationKeySecret()),
            child: const ConversationPage(),
          ),
        );

      case Routes.notification:
        return MaterialPageRoute(
          builder: (_) => BlocProvider(
            create: (context) => serviceLocator<NotificationBloc>()
              ..add(NotificationKeySecret()),
            child: const NotificationPage(),
          ),
        );

      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(
            body: Center(
              child: Text('No route defined for ${settings.name}'),
            ),
          ),
        );
    }
  }

  static SlideTransition _transitionBuilder(
      Animation<double> animation, Widget child) {
    return SlideTransition(
      position: Tween<Offset>(
        begin: const Offset(1.0, 0.0),
        end: const Offset(0.0, 0.0),
      ).animate(animation),
      child: child,
    );
  }
}

class Routes {
  static const String splash = "splash";
  static const String register = "register";
  static const String chat = "chat";
  static const String conversation = "conversation";
  static const String notification = "notification";
}
