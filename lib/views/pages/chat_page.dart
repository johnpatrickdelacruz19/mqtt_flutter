import 'package:cass_rnd/widget/chat/chat_screen.dart';
import 'package:flutter/material.dart';

class ChatPage extends StatelessWidget {
  const ChatPage({Key? key, required this.roomId, required this.name})
      : super(key: key);

  final String roomId;
  final String name;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ChatScreen(roomId: roomId, name: name),
    );
  }
}
