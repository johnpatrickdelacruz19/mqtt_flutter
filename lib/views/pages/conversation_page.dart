import 'package:cass_rnd/widget/conversation/conversation_screen.dart';
import 'package:flutter/material.dart';

class ConversationPage extends StatelessWidget {
  const ConversationPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: ConversationScreen(),
    );
  }
}
