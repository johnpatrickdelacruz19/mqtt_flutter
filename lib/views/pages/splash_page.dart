import 'dart:async';
import 'package:cass_rnd/bloc/navigation/navigation_bloc.dart';
import 'package:cass_rnd/bloc/navigation/navigation_event.dart';
import 'package:cass_rnd/bloc/splash/splash_bloc.dart';
import 'package:cass_rnd/bloc/splash/splash_state.dart';
import 'package:cass_rnd/views/generic/dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../values/dimens.dart' as dimens;
import 'package:responsive_sizer/responsive_sizer.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(seconds: dimens.splashDuration), () async {})
        .catchError((error) => throw Exception(error));
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SplashBloc, SplashState>(
      listener: (context, state) {
        if (state is SplashMaintenance) {
          if (state.isMaintenance != null) {
            // show dialog if maintenance
            if (state.isMaintenance!) {
              showDialog(
                barrierDismissible: false,
                context: context,
                builder: (BuildContext context) {
                  return const CustomDialogBox(
                    title: "App is under maintenance",
                    descriptions: "Kindly contact the admin for assistance.",
                  );
                },
              );
            }
          }

          if (state.isVersionEqual != null) {
            if (!state.isVersionEqual!) {
              showDialog(
                barrierDismissible: false,
                context: context,
                builder: (BuildContext context) {
                  return const CustomDialogBox(
                    title: "App is outdated",
                    descriptions: "Kindly contact the admin for assistance.",
                  );
                },
              );
            }
          }
        }

        if (state is SplashSuccess) {
          BlocProvider.of<NavigationBloc>(context)
              .add(const NavigationToRegister(predicate: true));
        }
      },
      child: BlocBuilder<SplashBloc, SplashState>(
        builder: (context, state) {
          return Scaffold(
            body: Center(
              child: Icon(
                Icons.adb,
                color: Colors.green,
                size: 25.h,
              ),
            ),
          );
        },
      ),
    );
  }
}
