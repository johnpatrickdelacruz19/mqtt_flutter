import 'package:cass_rnd/bloc/navigation/navigation_bloc.dart';
import 'package:cass_rnd/network/services/navigation_services.dart';
import 'package:cass_rnd/views/router.dart';
import 'package:device_preview/device_preview.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../locator.dart';
import 'package:cass_rnd/values/themes.dart' as themes;
import 'package:responsive_sizer/responsive_sizer.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<NavigationBloc>(
      create: (_) => NavigationBloc(),
      child: ResponsiveSizer(builder: (context, orientation, screenType) {
        return MaterialApp(
          builder: DevicePreview.appBuilder,
          navigatorKey: serviceLocator<NavigationService>().navigatorKey,
          theme: themes.lightThemeData,
          debugShowCheckedModeBanner: false,
          initialRoute: Routes.splash,
          onGenerateRoute: RouterManager.generateRoute,
        );
      }),
    );
  }
}
