import 'dart:convert';
import 'dart:io';

class SocketUtil {
  late Socket _socket;
  bool socketInit = false;
  static const String SERVER_IP = "192.168.1.10"; // base_url
  static const int SERVER_PORT = 3000; // where i can get this?

  Future<bool> sendMessage(String message) async {
    // via api
    try {
      _socket.add(utf8.encode(message));
    } catch (e) {
      print(e.toString());
      return false;
    }

    // user: sender
    return true;
  }

  Future<bool> initSocket(
      Function connectListener, Function messageListener) async {
    try {
      print('Connecting to socket');
      _socket = await Socket.connect(SERVER_IP, SERVER_PORT);
      connectListener(true);
      _socket.listen((List<int> event) {
        messageListener(utf8.decode(event));
      });
      socketInit = true;
    } catch (e) {
      print(e.toString());
      connectListener(false);
      return false;
    }
    return true;
  }

  void closeSocket() {
    _socket.close();
  }

  void cleanUp() {
    _socket.destroy();
  }
}
