import 'package:cass_rnd/bloc/chat/chat_event.dart';
import 'package:cass_rnd/bloc/chat/chat_state.dart';
import 'package:cass_rnd/locator.dart';
import 'package:cass_rnd/network/abstract_repositories/chat_repository_providing.dart';
import 'package:cass_rnd/network/helper/connectivity_helper.dart';
import 'package:cass_rnd/network/helper/errors.dart';
import 'package:cass_rnd/network/repositories/chat_repository.dart';
import 'package:cass_rnd/parser/http_response.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:web_socket_channel/io.dart';
import 'package:web_socket_channel/status.dart' as status;

class ChatBloc extends Bloc<ChatEvent, ChatState> {
  ChatBloc() : super(initialState);

  static ChatState get initialState => ChatInitial();

  final ChatRepositoryProviding _chatRepository =
      serviceLocator<ChatRepository>();

  String subscriptionId = '';

  @override
  Stream<ChatState> mapEventToState(ChatEvent event) async* {
    if (event is SubscribeEvent) {
      HttpResponse response =
          await _chatRepository.roomListen(roomId: event.roomId);

      if (!(await ConnectionHelper.hasConnection())) {
        yield const ChatFailure(error: Errors.NoNetwork);
        return;
      }

      switch (response.status) {
        case 200:
          yield ChatListenRoomSuccess();
          subscriptionId = response.data['data']['subscription_id'];
          print(subscriptionId);
          break;
        case 401:
          yield const ChatFailure(error: Errors.Generic);
          break;
        case 404:
          yield const ChatFailure(error: Errors.ApplicationNotFound);
          break;
        default:
          yield const ChatFailure(error: Errors.Generic);
          break;
      }
    }

    if (event is SendMessageEvent) {
      HttpResponse response = await _chatRepository.sendMessage(
        roomId: event.roomId,
        subscriptionId: subscriptionId,
        message: event.message,
      );

      if (!(await ConnectionHelper.hasConnection())) {
        yield const ChatFailure(error: Errors.NoNetwork);
        return;
      }

      switch (response.status) {
        case 200:
          yield ChatListenRoomSuccess();
          break;
        case 401:
          yield const ChatFailure(error: Errors.Generic);
          break;
        case 404:
          yield const ChatFailure(error: Errors.ApplicationNotFound);
          break;
        default:
          yield const ChatFailure(error: Errors.Generic);
          break;
      }
    }
  }
}
