import 'package:equatable/equatable.dart';

abstract class ChatEvent extends Equatable {
  const ChatEvent();

  @override
  List<Object> get props => [];
}

class SubscribeEvent extends ChatEvent {
  const SubscribeEvent({required this.roomId});

  final String roomId;

  @override
  List<Object> get props => [roomId];

  @override
  String toString() => 'SubscribeEvent: {roomId: $roomId}';
}

class UnsubscribeEvent extends ChatEvent {}

class SendMessageEvent extends ChatEvent {
  const SendMessageEvent({required this.roomId, required this.message});

  final String roomId;
  final String message;

  @override
  List<Object> get props => [roomId, message];

  @override
  String toString() =>
      'SendMessageEvent: {roomId: $roomId} {message: $message}';
}
