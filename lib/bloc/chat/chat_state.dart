import 'package:cass_rnd/network/helper/errors.dart';
import 'package:equatable/equatable.dart';

abstract class ChatState extends Equatable {
  const ChatState();

  @override
  List<Object> get props => [];
}

class ChatInitial extends ChatState {}

class ChatListenRoomSuccess extends ChatState {}

class ChatFailure extends ChatState {
  const ChatFailure({required this.error});

  final Errors error;

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ChatFailure {error: $error}';
}
