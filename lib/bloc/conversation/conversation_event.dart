import 'package:equatable/equatable.dart';

abstract class ConversationEvent extends Equatable {
  const ConversationEvent();

  @override
  List<Object> get props => [];
}

class ConversationListLoad extends ConversationEvent {}

class ConversationAddName extends ConversationEvent {
  const ConversationAddName({required this.name});

  final String name;

  @override
  List<Object> get props => [name];

  @override
  String toString() => 'ConversationAddName {name: $name}';
}

class ConversationKeySecret extends ConversationEvent {}
