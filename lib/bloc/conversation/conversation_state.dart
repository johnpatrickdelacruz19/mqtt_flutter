import 'package:cass_rnd/model/room.dart';
import 'package:cass_rnd/network/helper/errors.dart';
import 'package:equatable/equatable.dart';

abstract class ConversationState extends Equatable {
  const ConversationState();

  @override
  List<Object> get props => [];
}

class ConversationInitial extends ConversationState {}

class ConversationAddSuccess extends ConversationState {}

class ConversationListSuccess extends ConversationState {
  const ConversationListSuccess({required this.roomList});

  final List<Room> roomList;

  @override
  List<Object> get props => [roomList];

  @override
  String toString() => 'ConversationListSuccess {roomList: $roomList}';
}

class ConversationFailure extends ConversationState {
  const ConversationFailure({required this.error});

  final Errors error;

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ConversationFailure {error: $error}';
}
