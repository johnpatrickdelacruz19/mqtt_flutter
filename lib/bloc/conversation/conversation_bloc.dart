import 'package:cass_rnd/bloc/conversation/conversation_event.dart';
import 'package:cass_rnd/bloc/conversation/conversation_state.dart';
import 'package:cass_rnd/locator.dart';
import 'package:cass_rnd/network/abstract_repositories/chat_repository_providing.dart';
import 'package:cass_rnd/network/abstract_repositories/splash_repository_providing.dart';
import 'package:cass_rnd/network/helper/connectivity_helper.dart';
import 'package:cass_rnd/network/helper/errors.dart';
import 'package:cass_rnd/network/repositories/chat_repository.dart';
import 'package:cass_rnd/network/repositories/splash_repository.dart';
import 'package:cass_rnd/network/services/splash_service.dart';
import 'package:cass_rnd/parser/http_response.dart';
import 'package:cass_rnd/utility/storage_helper.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ConversationBloc extends Bloc<ConversationEvent, ConversationState> {
  ConversationBloc() : super(initialState);

  static ConversationState get initialState => ConversationInitial();

  final ChatRepositoryProviding _chatRepository =
      serviceLocator<ChatRepository>();
  final SplashRepositoryProviding _splashRepository =
      serviceLocator<SplashRepository>();

  @override
  Stream<ConversationState> mapEventToState(ConversationEvent event) async* {
    if (event is ConversationListLoad) {
      if (!(await ConnectionHelper.hasConnection())) {
        yield const ConversationFailure(error: Errors.NoNetwork);
        return;
      }
    } else if (event is ConversationAddName) {
      if (!(await ConnectionHelper.hasConnection())) {
        yield const ConversationFailure(error: Errors.NoNetwork);
        return;
      }

      HttpResponse response;
      response = await _chatRepository.roomAdd(roomName: event.name);

      switch (response.status) {
        case 201:
          yield ConversationAddSuccess();
          break;
        case 401:
          yield const ConversationFailure(error: Errors.Generic);
          break;

        default:
          yield const ConversationFailure(error: Errors.Generic);
          break;
      }
    } else if (event is ConversationKeySecret) {
      HttpResponse response;

      if (!(await ConnectionHelper.hasConnection())) {
        yield const ConversationFailure(error: Errors.NoNetwork);
        return;
      }

      response = await _splashRepository.clientAuth(authEnum: AuthEnum.chat);

      switch (response.status) {
        case 200:
          StorageHelper.set(
              StorageKeys.clientToken, response.data['access_token']);

          response = await _splashRepository.adminAuth(authEnum: AuthEnum.chat);

          switch (response.status) {
            case 200:
              StorageHelper.set(
                  StorageKeys.adminToken, response.data['access_token']);

              response = await _chatRepository.roomList();

              switch (response.status) {
                case 200:
                  print("success get room list");
                  yield ConversationListSuccess(roomList: response.data.data);
                  break;
                case 401:
                  yield const ConversationFailure(error: Errors.Generic);
                  break;

                default:
                  yield const ConversationFailure(error: Errors.Generic);
                  break;
              }

              break;
            case 401:
              yield const ConversationFailure(error: Errors.Generic);
              break;

            default:
              yield const ConversationFailure(error: Errors.Generic);
              break;
          }
          break;
        case 401:
          yield const ConversationFailure(error: Errors.Generic);
          break;

        default:
          yield const ConversationFailure(error: Errors.Generic);
          break;
      }
    }
  }
}
