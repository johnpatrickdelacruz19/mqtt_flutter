import 'package:cass_rnd/network/helper/errors.dart';
import 'package:equatable/equatable.dart';

abstract class SplashState extends Equatable {
  const SplashState();

  @override
  List<Object> get props => [];
}

class SplashInitial extends SplashState {}

class SplashSuccess extends SplashState {}

class SplashMaintenance extends SplashState {
  const SplashMaintenance({this.isMaintenance, this.isVersionEqual});

  final bool? isMaintenance;
  final bool? isVersionEqual;

  @override
  List<Object> get props => [isMaintenance!, isVersionEqual!];

  @override
  String toString() =>
      'SplashMaintenance {isMaintenance: $isMaintenance} {isVersionEqual: $isVersionEqual}';
}

class SplashFailure extends SplashState {
  const SplashFailure({required this.error});

  final Errors error;

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'SplashFailure {error: $error}';
}
