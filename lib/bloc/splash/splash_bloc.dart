import 'package:cass_rnd/bloc/splash/splash_event.dart';
import 'package:cass_rnd/bloc/splash/splash_state.dart';
import 'package:cass_rnd/locator.dart';
import 'package:cass_rnd/network/abstract_repositories/splash_repository_providing.dart';
import 'package:cass_rnd/network/helper/connectivity_helper.dart';
import 'package:cass_rnd/network/helper/errors.dart';
import 'package:cass_rnd/network/repositories/splash_repository.dart';
import 'package:cass_rnd/network/services/splash_service.dart';
import 'package:cass_rnd/parser/http_response.dart';
import 'package:cass_rnd/utility/storage_helper.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:package_info_plus/package_info_plus.dart';

class SplashBloc extends Bloc<SplashEvent, SplashState> {
  SplashBloc() : super(initialState);

  static SplashState get initialState => SplashInitial();

  final SplashRepositoryProviding _splashRepository =
      serviceLocator<SplashRepository>();

  @override
  Stream<SplashState> mapEventToState(SplashEvent event) async* {
    if (event is MaintenanceEvent) {
      HttpResponse response;

      if (!(await ConnectionHelper.hasConnection())) {
        yield const SplashFailure(error: Errors.NoNetwork);
        return;
      }

      response = await _splashRepository.clientAuth(authEnum: AuthEnum.chat);

      switch (response.status) {
        case 200:
          StorageHelper.set(
              StorageKeys.clientToken, response.data['access_token']);
          break;
        case 401:
          yield const SplashFailure(error: Errors.Generic);
          break;

        default:
          yield const SplashFailure(error: Errors.Generic);
          break;
      }

      response = await _splashRepository.adminAuth(authEnum: AuthEnum.chat);

      switch (response.status) {
        case 200:
          StorageHelper.set(
              StorageKeys.adminToken, response.data['access_token']);
          break;
        case 401:
          yield const SplashFailure(error: Errors.Generic);
          break;

        default:
          yield const SplashFailure(error: Errors.Generic);
          break;
      }

      response = await _splashRepository.appMaintenance();

      switch (response.status) {
        case 200:
          if (response.data['data']['enabled']) {
            // if true show maintenance dialog
            yield SplashMaintenance(
                isMaintenance: response.data['data']['enabled']);
          } else {
            // call version number api
            HttpResponse response = await _splashRepository.appVersionNumber();

            PackageInfo packageInfo = await PackageInfo.fromPlatform();

            String version = packageInfo.version;

            bool isVersionEqual;

            version == response.data['data']['version']
                ? isVersionEqual = true
                : isVersionEqual = false;
            // version == '1.0.0' ? isVersionEqual = true : isVersionEqual = false;

            switch (response.status) {
              case 200:
                if (isVersionEqual) {
                  yield SplashSuccess();
                } else {
                  yield SplashMaintenance(isVersionEqual: isVersionEqual);
                }

                break;
              case 401:
                yield const SplashFailure(error: Errors.Generic);
                break;
              case 404:
                yield const SplashFailure(error: Errors.ApplicationNotFound);
                break;
            }
          }

          break;
        case 401:
          yield const SplashFailure(error: Errors.Generic);
          break;
        case 404:
          yield const SplashFailure(error: Errors.ApplicationNotFound);
          break;
        default:
          yield const SplashFailure(error: Errors.Generic);
          break;
      }
    }
  }
}
