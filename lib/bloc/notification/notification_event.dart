import 'package:equatable/equatable.dart';

abstract class NotificationEvent extends Equatable {
  const NotificationEvent();

  @override
  List<Object> get props => [];
}

class NotificationErrorEvent extends NotificationEvent {
  const NotificationErrorEvent({this.error});

  final String? error;

  @override
  String toString() => 'NotificationErrorEvent {error: $error}';
}

class NotificationLoad extends NotificationEvent {}

class NotificationSubscribe extends NotificationEvent {
  const NotificationSubscribe(
      {required this.applicationId, required this.topic});

  final String applicationId;
  final String topic;

  @override
  String toString() =>
      'NotificationSubscribe {applicationId: $applicationId} {topic: $topic}';
}

class NotificationUnsubscribe extends NotificationEvent {
  const NotificationUnsubscribe({required this.applicationId});

  final String applicationId;

  @override
  String toString() => 'NotificationUbsubscribe ';
}

class NotificationKeySecret extends NotificationEvent {}
