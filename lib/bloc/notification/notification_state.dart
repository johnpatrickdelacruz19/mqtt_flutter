import 'package:cass_rnd/network/helper/errors.dart';
import 'package:equatable/equatable.dart';

class NotificationState extends Equatable {
  const NotificationState();

  @override
  List<Object> get props => [];
}

class NotificationInitial extends NotificationState {}

class NotificationFailure extends NotificationState {
  const NotificationFailure({required this.error});

  final Errors error;

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'NotificationFailure {error: $error}';
}

class NotificationToken extends NotificationState {
  const NotificationToken({required this.token});

  final String token;

  @override
  List<Object> get props => [token];

  @override
  String toString() => 'NotificationToken {token: $token}';
}

class NotificationSubscribeSuccess extends NotificationState {
  const NotificationSubscribeSuccess({required this.response});

  final dynamic response;

  @override
  List<Object> get props => [response];

  @override
  String toString() => 'NotificationSubscribeSuccess {response: $response}';
}
