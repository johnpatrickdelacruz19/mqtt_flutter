import 'package:cass_rnd/bloc/notification/notification_event.dart';
import 'package:cass_rnd/bloc/notification/notification_state.dart';
import 'package:cass_rnd/locator.dart';
import 'package:cass_rnd/network/abstract_repositories/notification_repository_providing.dart';
import 'package:cass_rnd/network/abstract_repositories/splash_repository_providing.dart';
import 'package:cass_rnd/network/helper/connectivity_helper.dart';
import 'package:cass_rnd/network/helper/errors.dart';
import 'package:cass_rnd/network/helper/notification_client.dart';
import 'package:cass_rnd/network/repositories/notification_repository.dart';
import 'package:cass_rnd/network/repositories/splash_repository.dart';
import 'package:cass_rnd/network/services/splash_service.dart';
import 'package:cass_rnd/parser/http_response.dart';
import 'package:cass_rnd/utility/storage_helper.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

/// The NotificationBloc class is a Bloc that manages a NotificationState based on an
/// event triggered by a NotificationEvent or an extension of it

class NotificationBloc extends Bloc<NotificationEvent, NotificationState> {
  NotificationBloc() : super(initialState);

  final NotificationClient _notificationClient =
      serviceLocator<NotificationClient>();

  final NotificationRepositoryProviding _notificationRepository =
      serviceLocator<NotificationRepository>();
  final SplashRepositoryProviding _splashRepository =
      serviceLocator<SplashRepository>();

  static NotificationState get initialState => NotificationInitial();

  @override
  Stream<NotificationState> mapEventToState(NotificationEvent event) async* {
    if (event is NotificationLoad) {
      yield* _mapNotificationLoadToState();
    } else if (event is NotificationSubscribe) {
      if (!(await ConnectionHelper.hasConnection())) {
        yield const NotificationFailure(error: Errors.NoNetwork);
        return;
      }

      try {
        HttpResponse response =
            await _notificationRepository.notificationSubscribe(
                applicationId: event.applicationId, topic: event.topic);

        switch (response.status) {
          case 200:
            print("success");
            yield const NotificationSubscribeSuccess(response: 'subscribe');
            break;
          case 401:
            yield const NotificationFailure(error: Errors.Generic);
            break;

          default:
            yield const NotificationFailure(error: Errors.Generic);
            break;
        }
      } catch (error) {
        yield const NotificationFailure(error: Errors.Generic);
      }
    } else if (event is NotificationUnsubscribe) {
      if (!(await ConnectionHelper.hasConnection())) {
        yield const NotificationFailure(error: Errors.NoNetwork);
        return;
      }

      try {
        HttpResponse response = await _notificationRepository
            .notificationUnsubscribe(applicationId: event.applicationId);

        switch (response.status) {
          case 200:
            print("NotificationUbsubscribe success");
            yield const NotificationSubscribeSuccess(response: 'unsubscribe');
            break;
          case 401:
            yield const NotificationFailure(error: Errors.Generic);
            break;

          default:
            yield const NotificationFailure(error: Errors.Generic);
            break;
        }
      } catch (error) {
        yield const NotificationFailure(error: Errors.Generic);
      }
    } else if (event is NotificationKeySecret) {
      HttpResponse response;

      if (!(await ConnectionHelper.hasConnection())) {
        yield const NotificationFailure(error: Errors.NoNetwork);
        return;
      }

      response = await _splashRepository.clientAuth(authEnum: AuthEnum.notif);

      switch (response.status) {
        case 200:
          StorageHelper.set(
              StorageKeys.clientToken, response.data['access_token']);
          break;
        case 401:
          yield const NotificationFailure(error: Errors.Generic);
          break;

        default:
          yield const NotificationFailure(error: Errors.Generic);
          break;
      }

      response = await _splashRepository.adminAuth(authEnum: AuthEnum.notif);

      switch (response.status) {
        case 200:
          StorageHelper.set(
              StorageKeys.adminToken, response.data['access_token']);
          break;
        case 401:
          yield const NotificationFailure(error: Errors.Generic);
          break;

        default:
          yield const NotificationFailure(error: Errors.Generic);
          break;
      }
    }
  }

  /// The _mapNotificationLoadToState() function is triggered when the NotificationLoad event is triggered

  Stream<NotificationState> _mapNotificationLoadToState() async* {
    if (!(await ConnectionHelper.hasConnection())) {
      yield const NotificationFailure(error: Errors.NoNetwork);
      return;
    }
    try {
      final String token = await _notificationClient.init(
        context: null,
        callback: (message) async {},
      );

      if (token.isNotEmpty) {
        yield NotificationToken(token: token);
        // final HttpResponse response =
        // await _accountRepository.registerDevice(token: token);
        // switch (response.status) {
        // case 200:
        // yield NotificationState.successfullySetup(true, null);
        // break;
        // case 401: //invalid credentials
        // case 419: //invalid/expired tokens
        // case 422: //invalid parameters
        // yield NotificationState.successfullySetup(false, Error.ForceLogout);
        // break;
        // default:
        // yield NotificationState.successfullySetup(false, Error.Generic);
        // break;
      }
      // } else {
      // yield NotificationState.successfullySetup(true, null);
      // }
    } catch (error) {
      // utils.log(error);
      // yield NotificationState.successfullySetup(false, Error.Generic);
    }
  }
}
