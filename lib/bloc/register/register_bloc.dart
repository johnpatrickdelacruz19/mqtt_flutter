import 'package:cass_rnd/bloc/register/register_event.dart';
import 'package:cass_rnd/bloc/register/register_state.dart';
import 'package:cass_rnd/locator.dart';
import 'package:cass_rnd/network/abstract_repositories/auth_repository_providing.dart';
import 'package:cass_rnd/network/helper/connectivity_helper.dart';
import 'package:cass_rnd/network/helper/errors.dart';
import 'package:cass_rnd/network/repositories/auth_repository.dart';
import 'package:cass_rnd/parser/http_response.dart';
import 'package:cass_rnd/utility/storage_helper.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  RegisterBloc() : super(initialState);

  static RegisterState get initialState => RegisterInitial();

  final AuthRepositoryProviding _authRepository =
      serviceLocator<AuthRepository>();

  @override
  Stream<RegisterState> mapEventToState(RegisterEvent event) async* {
    if (event is RegisterUserEvent) {
      if (!(await ConnectionHelper.hasConnection())) {
        yield const RegisterFailure(error: Errors.NoNetwork);
        return;
      }

      HttpResponse response = await _authRepository.loginUser(
          username: event.email, password: event.password);

      switch (response.status) {
        case 200:
          StorageHelper.set(
              StorageKeys.userID, response.data['user_profile']['id']);
          yield RegisterSuccess();
          break;
        case 401:
          yield const RegisterFailure(error: Errors.Generic);
          break;

        default:
          yield const RegisterFailure(error: Errors.Generic);
          break;
      }
    }
  }
}
