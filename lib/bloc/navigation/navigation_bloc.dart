import 'package:cass_rnd/bloc/navigation/navigation_event.dart';
import 'package:cass_rnd/network/services/navigation_services.dart';
import 'package:cass_rnd/views/router.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../locator.dart';

class NavigationBloc extends Bloc<NavigationEvent, dynamic> {
  NavigationBloc() : super(initialState);

  final NavigationService _navigationService =
      serviceLocator<NavigationService>();

  static int get initialState => 0;

  dynamic arguments;

  @override
  Stream<dynamic> mapEventToState(NavigationEvent event) async* {
    if (event is NavigationToSplash) {
      _navigationService.navigateTo(Routes.splash, false);
    } else if (event is NavigationToRegister) {
      _navigationService.navigateTo(Routes.register, false);
    } else if (event is NavigationToChat) {
      _navigationService
          .navigateTo(Routes.chat, true, arguments: [event.roomId, event.name]);
    } else if (event is NavigationToConversation) {
      _navigationService.navigateTo(Routes.conversation, false);
    } else if (event is NavigationToNotifications) {
      _navigationService.navigateTo(Routes.notification, true);
    }
  }
}
