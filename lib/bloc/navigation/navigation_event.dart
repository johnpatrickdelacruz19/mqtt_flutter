import 'package:equatable/equatable.dart';

abstract class NavigationEvent extends Equatable {
  const NavigationEvent();

  @override
  List<Object> get props => [];
}

class NavigationToSplash extends NavigationEvent {}

class NavigationToRegister extends NavigationEvent {
  const NavigationToRegister({required this.predicate});

  final bool predicate;

  @override
  List<Object> get props => [predicate];

  @override
  String toString() => 'NavigationToRegister: {predicate: $predicate}';
}

class NavigationToChat extends NavigationEvent {
  const NavigationToChat({required this.roomId, required this.name});

  final String roomId;
  final String name;

  @override
  List<Object> get props => [roomId, name];

  @override
  String toString() => 'NavigationToChat {roomId: $roomId} {name: $name}';
}

class NavigationToConversation extends NavigationEvent {
  @override
  String toString() => 'NavigationToConversation';
}

class NavigationToNotifications extends NavigationEvent {
  @override
  String toString() => 'NavigationToNotifications';
}
