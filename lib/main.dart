import 'dart:async';

import 'package:cass_rnd/bloc/bloc_observer.dart';
import 'package:cass_rnd/bloc/chat/chat_bloc.dart';
import 'package:cass_rnd/bloc/conversation/conversation_bloc.dart';
import 'package:cass_rnd/bloc/navigation/navigation_bloc.dart';
import 'package:cass_rnd/bloc/notification/notification_bloc.dart';
import 'package:cass_rnd/bloc/register/register_bloc.dart';
import 'package:cass_rnd/bloc/splash/splash_bloc.dart';
import 'package:cass_rnd/locator.dart';
import 'package:cass_rnd/network/services/local_notification_service.dart';
import 'package:cass_rnd/views/app.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:device_preview/device_preview.dart' as dev_prev;

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await setupLocator();
  LocalNotificationService().init();

  Bloc.observer = SimpleBlocObserver();

  final List<BlocProvider> providers = [
    BlocProvider<NavigationBloc>(
        create: (_) => serviceLocator<NavigationBloc>()),
    BlocProvider<SplashBloc>(create: (_) => serviceLocator<SplashBloc>()),
    BlocProvider<NotificationBloc>(
        create: (_) => serviceLocator<NotificationBloc>()),
    BlocProvider<ChatBloc>(create: (_) => serviceLocator<ChatBloc>()),
    BlocProvider<RegisterBloc>(create: (_) => serviceLocator<RegisterBloc>()),
    BlocProvider<ConversationBloc>(
        create: (_) => serviceLocator<ConversationBloc>()),
  ];

  // await initializeAPI(true);

  runZonedGuarded<Future>(
    () async =>
        SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
            .then(
      (_) => runApp(dev_prev.DevicePreview(
        enabled: false,
        builder: (context) => MultiBlocProvider(
          providers: providers,
          child: const App(),
        ),
      )),
    ),
    (error, stackTrace) => print(error),
  );
}
