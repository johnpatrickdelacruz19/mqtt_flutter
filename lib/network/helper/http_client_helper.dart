import 'package:cass_rnd/utility/storage_helper.dart';
import 'package:dio/dio.dart';

class HttpClientHelper {
  late Dio _client;

  bool isAdmin = false;

  HttpClientHelper() {
    _client = Dio(BaseOptions(
      receiveDataWhenStatusError: true,
    ));

    _client.interceptors
        .add(InterceptorsWrapper(onRequest: (options, handler) async {
      await _attachAuthToken(options);

      return handler.next(options);
    }, onResponse: (response, handler) {
      return handler.next(response);
    }, onError: (DioError error, handler) async {
      return handler.next(error);
    }));
  }

  Future<RequestOptions> _attachAuthToken(RequestOptions options) async {
    dynamic accessToken;

    if (isAdmin) {
      accessToken = await StorageHelper.get(StorageKeys.adminToken);
    } else {
      accessToken = await StorageHelper.get(StorageKeys.clientToken);
      // accessToken =
      //     'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiMTY2ODdlYmYtMDVmYi00OWI1LWEwMzgtNjI5MjE3OGM5MjhlIiwidG9rZW4iOiJxMzNnSkRGZ1lpOURlMmJXTUgiLCJ0eXBlIjoiQ0xJRU5UIiwiaXNfcHJvZHVjdGlvbiI6ZmFsc2UsInNlc3Npb25faWQiOiI3NTdjMDk1Ny0wNDRhLTRlNGMtODk2Mi1iODg0ZGY3NGViYjUiLCJpYXQiOjE2MzkzNjIzNzIsImV4cCI6MTYzOTM2MzI3Mn0.RJqauoPqjVLR0qZQpKadL7OiEpgtsr5699FoHgLBFqGWnWZZNwUtaCkKM9bp0oL75_y7aH7szVqJ0R-SqBgB5BCKPNDhmSAWp2M3D74SX-1b_F96kBEfej1KiT6sjwW1E37g06-nSwk6Fk3YbuG4ocOrjaVL3zle8T2-HK-HGuRIcGraCxTfV5PubKU';
    }

    Map<String, dynamic> headers = {};
    headers['Charset'] = 'utf-8';
    headers['Content-Type'] = 'application/json;charset=UTF-8';
    if (accessToken != null) headers['Authorization'] = 'Bearer $accessToken';
    options.headers = headers;

    return options;
  }

  Future<Response> get(String url, {required bool isAdmin}) async {
    this.isAdmin = isAdmin;
    return await _client.get(url);
  }

  Future<Response> patch(String url, {dynamic body}) async {
    return _client.patch(url, data: body);
  }

  Future<Response> post(String url,
      {dynamic body, required bool isAdmin}) async {
    this.isAdmin = isAdmin;
    return _client.post(url, data: body);
  }

  Future<Response> put(String url,
      {dynamic body, required bool isAdmin}) async {
    this.isAdmin = isAdmin;
    return _client.put(url, data: body);
  }

  Future<Response> delete(String url,
      {dynamic body, required bool isAdmin}) async {
    this.isAdmin = isAdmin;
    return _client.delete(url);
  }
}
