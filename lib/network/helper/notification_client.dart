import 'dart:io' show Platform;

import 'package:cass_rnd/network/services/local_notification_service.dart';
import 'package:cass_rnd/utility/storage_helper.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../values/strings.dart' as strings;

typedef NotificationCallback = void Function(Map<String, dynamic> message);

/// The NotificationClient class is a client class responsible for connecting the app to the Firebase notification
/// handler. This is mainly triggered every time the user visits the dashboard

class NotificationClient {
  final LocalNotificationService _notificationService =
      LocalNotificationService();
  factory NotificationClient() => _instance;

  NotificationClient._();

  static final NotificationClient _instance = NotificationClient._();

  Future<String> init(
      {BuildContext? context, NotificationCallback? callback}) async {
    try {
      // workaround for onLaunch: When the app is completely closed (not in the background) and opened directly from the push notification
      FirebaseMessaging.instance
          .getInitialMessage()
          .then((RemoteMessage? message) {
        if (message != null) {
          print('getInitialMessage data: ${message.data}');
        }
      });

      // onMessage: When the app is open and it receives a push notification
      FirebaseMessaging.onMessage.listen((RemoteMessage message) async {
        print(message.notification!.title);
        print(message.notification!.body);
        print("onMessage data: ${message.data}");
        await _notificationService.showNotifications(
            title: message.notification!.title,
            body: message.notification!.body);
      });

      // replacement for onResume: When the app is in the background and opened directly from the push notification.
      FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
        print('onMessageOpenedApp data: ${message.data}');
        // _serialiseAndNavigate(message);
      });

      String token = '';
      await FirebaseMessaging.instance.getToken().then((value) {
        token = value!;
      });

      print("FirebaseMessaging token: $token");
      StorageHelper.set(StorageKeys.firebaseToken, token);

      // final String storedToken =
      //     await StorageHelper.get(StorageKeys.firebaseToken);
      // if (token != storedToken) {
      //   await StorageHelper.set(StorageKeys.firebaseToken, token);
      // } else {
      //   token = '';
      // }
      return token;
    } on Exception {
      return '';
    } catch (error) {
      return '';
    }
  }
}
