import '../../values/strings.dart' as strings;

enum Errors {
  Generic,
  NoNetwork,
  ApplicationNotFound,
}

extension ErrorExtension on Errors {
  String get errorMessage {
    switch (this) {
      case Errors.Generic:
        return strings.errorGeneric;
      case Errors.NoNetwork:
        return strings.errorNoInternet;
      case Errors.ApplicationNotFound:
        return strings.applicationNotFound;

      default:
        return strings.errorGeneric;
    }
  }
}
