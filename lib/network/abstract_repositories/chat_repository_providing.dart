import 'package:cass_rnd/parser/http_response.dart';

abstract class ChatRepositoryProviding {
  Future<HttpResponse> roomListen({roomId});

  Future<HttpResponse> roomList();

  Future<HttpResponse> roomAdd({required String roomName});

  Future<HttpResponse> sendMessage(
      {required String roomId, required subscriptionId, required message});
}
