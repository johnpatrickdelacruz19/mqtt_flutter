import 'package:cass_rnd/parser/http_response.dart';

abstract class SplashRepositoryProviding {
  Future<HttpResponse> appMaintenance();

  Future<HttpResponse> appVersionNumber();

  Future<HttpResponse> clientAuth({required authEnum});

  Future<HttpResponse> adminAuth({required authEnum});
}
