import 'package:cass_rnd/parser/http_response.dart';

abstract class NotificationRepositoryProviding {
  Future<HttpResponse> notificationSubscribe({applicationId, topic});

  Future<HttpResponse> notificationUnsubscribe({applicationId});
}
