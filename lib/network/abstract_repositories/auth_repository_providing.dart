import 'package:cass_rnd/parser/http_response.dart';

abstract class AuthRepositoryProviding {
  Future<HttpResponse> loginUser({username, password});
}
