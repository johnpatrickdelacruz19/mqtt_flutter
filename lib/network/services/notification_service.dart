import 'package:cass_rnd/network/helper/http_client_helper.dart';
import 'package:cass_rnd/parser/http_response.dart';
import 'package:cass_rnd/utility/storage_helper.dart';
import 'package:device_info/device_info.dart';
import 'package:dio/dio.dart';

import '../../locator.dart';

class NotificationService {
  final httpClientHelper = serviceLocator<HttpClientHelper>();

  Future<HttpResponse> notificationSubscribe({applicationId, topic}) async {
    final HttpResponse response = HttpResponse();

    final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
    final AndroidDeviceInfo androidDeviceInfo =
        await deviceInfoPlugin.androidInfo;

    var url =
        'https://api-dev.917v.com.ph/push_notification/application/$applicationId/subscribe';

    final payload = {
      "user_id": await StorageHelper.get(StorageKeys.userID),
      "topic": topic,
      "device_token": await StorageHelper.get(StorageKeys.firebaseToken),
      "device_type": "android",
      "os_version": androidDeviceInfo.version.sdkInt.toString(),
      "brand": androidDeviceInfo.manufacturer,
      "model": androidDeviceInfo.model
    };

    final resp = httpClientHelper.post(url, body: payload, isAdmin: false);

    await resp.then((res) {
      response.status = res.statusCode;
      response.message = res.statusMessage;
      response.data = res.data;
    }).catchError((e) {
      if (e is DioError) {
        final res = e.response;
        response.status = res!.statusCode;
      } else {
        response.status = 500;
        response.data = e;
      }
    });

    return response;
  }

  Future<HttpResponse> notificationUnsubscribe({applicationId}) async {
    final HttpResponse response = HttpResponse();

    var firebaseToken = await StorageHelper.get(StorageKeys.firebaseToken);

    var url =
        'https://api-dev.917v.com.ph/push_notification/application/$applicationId/device/android/$firebaseToken/unsubscribe';

    final resp = httpClientHelper.delete(url, isAdmin: false);

    await resp.then((res) {
      response.status = res.statusCode;
      response.message = res.statusMessage;
      response.data = res.data;
    }).catchError((e) {
      if (e is DioError) {
        final res = e.response;
        response.status = res!.statusCode;
      } else {
        response.status = 500;
        response.data = e;
      }
    });

    return response;
  }
}
