import 'package:cass_rnd/network/helper/http_client_helper.dart';
import 'package:cass_rnd/parser/http_response.dart';
import 'package:cass_rnd/parser/room_response.dart';
import 'package:cass_rnd/utility/storage_helper.dart';
import 'package:dio/dio.dart';

import '../../locator.dart';

class ChatService {
  final httpClientHelper = serviceLocator<HttpClientHelper>();

  Future<HttpResponse> roomListen({required String roomId}) async {
    final HttpResponse response = HttpResponse();

    var url = 'https://api-dev.917v.com.ph/chat/rooms/$roomId/listen';

    final payload = {"user_id": await StorageHelper.get(StorageKeys.userID)};

    print(payload);

    final resp = httpClientHelper.put(url, body: payload, isAdmin: true);

    await resp.then((res) {
      response.status = res.statusCode;
      response.message = res.statusMessage;
      response.data = res.data;
    }).catchError((e) {
      if (e is DioError) {
        final res = e.response;
        response.status = res!.statusCode;
      } else {
        response.status = 500;
        response.data = e;
      }
    });

    return response;
  }

  Future<HttpResponse> roomList() async {
    // final HttpResponse response = HttpResponse();

    // var accessToken = await StorageHelper.get(StorageKeys.adminToken);

    // var dio = Dio();
    // dio.options.headers["Authorization"] = "Bearer $accessToken";

    // final resp = await dio.get('https://api-dev.917v.com.ph/chat/rooms');
    // print(resp.data);

    // resp.statusCode;

    // try {
    //   response.status = resp.statusCode;
    //   response.message = resp.statusMessage;

    //   if (resp.statusCode == 200) {
    //     response.data = RoomsResponse.fromJson(
    //       resp.data,
    //     );
    //   }
    // } catch (e) {
    //   if (e is DioError) {
    //     final res = e.response;
    //     response.status = res!.statusCode;
    //   } else {
    //     response.status = 500;
    //     response.data = e;
    //   }
    // }
    // return response;

    final HttpResponse response = HttpResponse();

    const url = 'https://api-dev.917v.com.ph/chat/rooms';
    // const url = 'https://reqres.in/api/users/2';
    print("45");
    var resp = await httpClientHelper.get(url, isAdmin: true);
    print("46");
    print(resp);

    // Future.delayed(const Duration(seconds: 1), () {
    try {
      response.status = resp.statusCode;
      response.message = resp.statusMessage;

      if (resp.statusCode == 200) {
        response.data = RoomsResponse.fromJson(
          resp.data,
        );
      }
    } catch (e) {
      if (e is DioError) {
        final res = e.response;
        response.status = res!.statusCode;
      } else {
        response.status = 500;
        response.data = e;
      }
    }

    return response;

    // resp.then((res) {
    // response.status = res.statusCode;
    // response.message = res.statusMessage;

    // if (res.statusCode == 200) {
    //   response.data = RoomsResponse.fromJson(
    //     res.data,
    //   );
    // }
    // }).catchError((e) {

    // });
    // });

    // return response;
  }

  Future<HttpResponse> roomAdd({required String roomName}) async {
    final HttpResponse response = HttpResponse();

    const url = 'https://api-dev.917v.com.ph/chat/rooms';

    final resp =
        httpClientHelper.post(url, body: {"name": roomName}, isAdmin: true);

    await resp.then((res) {
      response.status = res.statusCode;
      response.message = res.statusMessage;
      response.data = res.data;
    }).catchError((e) {
      if (e is DioError) {
        final res = e.response;
        response.status = res!.statusCode;
      } else {
        response.status = 500;
        response.data = e;
      }
    });

    return response;
  }

  Future<HttpResponse> sendMessage(
      {required String roomId,
      required subscriptionId,
      required message}) async {
    final HttpResponse response = HttpResponse();

    const url = 'https://api-dev.917v.com.ph/chat/chats/send';
    //  "subscription_id": subscriptionId,
    final payload = {
      "room_id": roomId,
      "user_id": await StorageHelper.get(StorageKeys.userID),
      "user_name": "user zero one",
      "message": message
    };

    final resp = httpClientHelper.post(url, body: payload, isAdmin: false);

    await resp.then((res) {
      response.status = res.statusCode;
      response.message = res.statusMessage;
      response.data = res.data;
    }).catchError((e) {
      if (e is DioError) {
        final res = e.response;
        response.status = res!.statusCode;
      } else {
        response.status = 500;
        response.data = e;
      }
    });

    return response;
  }
}
