import 'package:cass_rnd/network/helper/http_client_helper.dart';
import 'package:cass_rnd/parser/http_response.dart';
import 'package:dio/dio.dart';

import '../../locator.dart';

class AuthService {
  final httpClientHelper = serviceLocator<HttpClientHelper>();

  Future<HttpResponse> loginUser({username, password}) async {
    final HttpResponse response = HttpResponse();

    const url = 'https://api-dev.917v.com.ph/users/login';

    final payload = {"username": username, "password": password};

    final resp = httpClientHelper.post(url, body: payload, isAdmin: false);

    await resp.then((res) {
      response.status = res.statusCode;
      response.message = res.statusMessage;
      response.data = res.data;
    }).catchError((e) {
      if (e is DioError) {
        final res = e.response;
        response.status = res!.statusCode;
      } else {
        response.status = 500;
        response.data = e;
      }
    });

    return response;
  }
}
