import 'package:cass_rnd/network/helper/http_client_helper.dart';
import 'package:cass_rnd/parser/http_response.dart';
import 'package:dio/dio.dart';

import '../../locator.dart';

class SplashService {
  final httpClientHelper = serviceLocator<HttpClientHelper>();

  Future<HttpResponse> appMaintenance() async {
    final HttpResponse response = HttpResponse();

    // const url = 'https://run.mocky.io/v3/fecf794d-faea-46d5-a4d3-4305f3b68863';
    const url =
        'https://blurmw7f9a.execute-api.ap-southeast-1.amazonaws.com/dev/client/application/a1507f14-4bba-493f-a403-eb759038388e/maintenance';

    final resp = httpClientHelper.get(url, isAdmin: false);

    await resp.then((res) {
      response.status = res.statusCode;
      response.message = res.statusMessage;
      response.data = res.data;
    }).catchError((e) {
      if (e is DioError) {
        final res = e.response;
        response.status = res!.statusCode;
      } else {
        response.status = 500;
        response.data = e;
      }
    });

    return response;
  }

  Future<HttpResponse> appVersionNumber() async {
    final HttpResponse response = HttpResponse();

    // const url = 'https://run.mocky.io/v3/416f934b-99e5-46a9-9bad-2136a8df0a9a';
    const url =
        'https://blurmw7f9a.execute-api.ap-southeast-1.amazonaws.com/dev/client/application/a1507f14-4bba-493f-a403-eb759038388e/platform/android/version';

    final resp = httpClientHelper.get(url, isAdmin: false);

    await resp.then((res) {
      response.status = res.statusCode;
      response.message = res.statusMessage;
      response.data = res.data;
    }).catchError((e) {
      if (e is DioError) {
        final res = e.response;
        response.status = res!.statusCode;
      } else {
        response.status = 500;
        response.data = e;
      }
    });

    return response;
  }

  Future<HttpResponse> clientAuth({required AuthEnum authEnum}) async {
    final HttpResponse response = HttpResponse();

    const url = 'https://api-dev.917v.com.ph/wallets/auth';
    dynamic payload;
    switch (authEnum) {
      case AuthEnum.chat:
        {
          payload = {
            "key": "gjUsBzNeYkTKzJxcSd",
            "secret":
                "zAmAjDifcx6EmZNuISpsbxZIvp8AdFB8m0O0EOjpgFr0HKAbpD6djladt5WVCXbRe0aT4xtWG4XtmkTxuS6qOEvt"
          };
        }
        break;

      case AuthEnum.notif:
        {
          payload = {
            "key": "8y4bpMeQMAwVKvIGkp",
            "secret":
                "oblD3rRI8oUZ26Ij32ZBxnsy2zgLIVPM8RxeM2QKyORFJ8FColqvcAxfGO7CIXMVf8M40dtLAvUIiBOCL82F43wn"
          };
        }
        break;
      case AuthEnum.maintenance:
        // TODO: Handle this case.
        break;
    }

    final resp = httpClientHelper.post(url, body: payload, isAdmin: false);

    await resp.then((res) {
      response.status = res.statusCode;
      response.message = res.statusMessage;
      response.data = res.data;
    }).catchError((e) {
      if (e is DioError) {
        final res = e.response;
        response.status = res!.statusCode;
      } else {
        response.status = 500;
        response.data = e;
      }
    });

    return response;
  }

  Future<HttpResponse> adminAuth({required AuthEnum authEnum}) async {
    final HttpResponse response = HttpResponse();

    const url = 'https://api-dev.917v.com.ph/wallets/auth';
    dynamic payload;

    switch (authEnum) {
      case AuthEnum.chat:
        {
          payload = {
            "key": "jH3HWO86K5Ig8JUQSi",
            "secret":
                "P0wzh6vYcJcbp9nrdjn6phoX9gmGk4lpf4zPAAHRguDcmVs93b9iCVNjOwxfDabhdcBZrqcIMmvLjoL8eHbSg5P1"
          };
        }
        break;

      case AuthEnum.notif:
        {
          payload = {
            "key": "c6a1VjXwQgBHiphQAE",
            "secret":
                "fryESFIsfKz0UUgRqgh3lD3kuDVsU1KAnI12W3HzeK8SFLIOu6Lyo6YTad3WA3n0IcWEJVeFQch3uUSuKhM151QI"
          };
        }
        break;
      case AuthEnum.maintenance:
        // TODO: Handle this case.
        break;
    }

    final resp = httpClientHelper.post(url, body: payload, isAdmin: false);

    await resp.then((res) {
      response.status = res.statusCode;
      response.message = res.statusMessage;
      response.data = res.data;
    }).catchError((e) {
      if (e is DioError) {
        final res = e.response;
        response.status = res!.statusCode;
      } else {
        response.status = 500;
        response.data = e;
      }
    });

    return response;
  }
}

enum AuthEnum { chat, notif, maintenance }
