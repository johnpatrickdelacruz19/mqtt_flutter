import 'package:cass_rnd/network/abstract_repositories/auth_repository_providing.dart';
import 'package:cass_rnd/network/services/auth_service.dart';
import 'package:cass_rnd/parser/http_response.dart';

import '../../locator.dart';

class AuthRepository extends AuthRepositoryProviding {
  final AuthService _authService = serviceLocator<AuthService>();

  @override
  Future<HttpResponse> loginUser({username, password}) async =>
      await _authService.loginUser(username: username, password: password);
}
