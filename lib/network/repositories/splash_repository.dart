import 'package:cass_rnd/network/abstract_repositories/splash_repository_providing.dart';
import 'package:cass_rnd/network/services/splash_service.dart';
import 'package:cass_rnd/parser/http_response.dart';

import '../../locator.dart';

class SplashRepository extends SplashRepositoryProviding {
  final SplashService _splashService = serviceLocator<SplashService>();

  @override
  Future<HttpResponse> appMaintenance() async =>
      await _splashService.appMaintenance();

  @override
  Future<HttpResponse> appVersionNumber() async =>
      await _splashService.appVersionNumber();

  @override
  Future<HttpResponse> clientAuth({required authEnum}) async =>
      await _splashService.clientAuth(authEnum: authEnum);

  @override
  Future<HttpResponse> adminAuth({required authEnum}) async =>
      await _splashService.adminAuth(authEnum: authEnum);
}
