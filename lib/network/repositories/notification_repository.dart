import 'package:cass_rnd/network/abstract_repositories/notification_repository_providing.dart';
import 'package:cass_rnd/network/services/auth_service.dart';
import 'package:cass_rnd/network/services/notification_service.dart';
import 'package:cass_rnd/parser/http_response.dart';

import '../../locator.dart';

class NotificationRepository extends NotificationRepositoryProviding {
  final NotificationService _notificationService =
      serviceLocator<NotificationService>();

  @override
  Future<HttpResponse> notificationSubscribe({applicationId, topic}) async =>
      await _notificationService.notificationSubscribe(
          applicationId: applicationId, topic: topic);

  @override
  Future<HttpResponse> notificationUnsubscribe({applicationId}) async =>
      await _notificationService.notificationUnsubscribe(
          applicationId: applicationId);
}
