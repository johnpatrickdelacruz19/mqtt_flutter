import 'package:cass_rnd/network/abstract_repositories/chat_repository_providing.dart';
import 'package:cass_rnd/network/services/chat_service.dart';
import 'package:cass_rnd/parser/http_response.dart';

import '../../locator.dart';

class ChatRepository extends ChatRepositoryProviding {
  final ChatService _chatService = serviceLocator<ChatService>();

  @override
  Future<HttpResponse> roomListen({roomId}) async =>
      await _chatService.roomListen(roomId: roomId);

  @override
  Future<HttpResponse> roomList() async => await _chatService.roomList();

  @override
  Future<HttpResponse> roomAdd({required String roomName}) async =>
      await _chatService.roomAdd(roomName: roomName);

  @override
  Future<HttpResponse> sendMessage(
          {required String roomId,
          required subscriptionId,
          required message}) async =>
      await _chatService.sendMessage(
          roomId: roomId, subscriptionId: subscriptionId, message: message);
}
