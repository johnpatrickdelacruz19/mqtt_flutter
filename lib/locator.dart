import 'package:cass_rnd/bloc/chat/chat_bloc.dart';
import 'package:cass_rnd/bloc/conversation/conversation_bloc.dart';
import 'package:cass_rnd/bloc/notification/notification_bloc.dart';
import 'package:cass_rnd/bloc/register/register_bloc.dart';
import 'package:cass_rnd/bloc/splash/splash_bloc.dart';
import 'package:cass_rnd/network/helper/http_client_helper.dart';
import 'package:cass_rnd/network/repositories/auth_repository.dart';
import 'package:cass_rnd/network/repositories/chat_repository.dart';
import 'package:cass_rnd/network/repositories/notification_repository.dart';
import 'package:cass_rnd/network/repositories/splash_repository.dart';
import 'package:cass_rnd/network/services/auth_service.dart';
import 'package:cass_rnd/network/services/chat_service.dart';
import 'package:cass_rnd/network/services/local_notification_service.dart';
import 'package:cass_rnd/network/services/notification_service.dart';

import 'package:cass_rnd/network/services/splash_service.dart';
import 'package:cass_rnd/network/services/navigation_services.dart';
import 'package:get_it/get_it.dart';

import 'network/helper/notification_client.dart';

final GetIt serviceLocator = GetIt.I;

Future setupLocator() async {
  serviceLocator
      .registerLazySingleton<HttpClientHelper>(() => HttpClientHelper());
  serviceLocator.registerSingleton<NotificationClient>(NotificationClient());

  serviceLocator
      .registerLazySingleton<NavigationService>(() => NavigationService());
  serviceLocator.registerLazySingleton<SplashService>(() => SplashService());
  serviceLocator.registerLazySingleton<ChatService>(() => ChatService());
  serviceLocator.registerLazySingleton<AuthService>(() => AuthService());
  serviceLocator
      .registerLazySingleton<NotificationService>(() => NotificationService());
  serviceLocator.registerFactory<LocalNotificationService>(
      () => LocalNotificationService());

  serviceLocator.registerFactory<SplashRepository>(() => SplashRepository());
  serviceLocator.registerFactory<ChatRepository>(() => ChatRepository());
  serviceLocator.registerFactory<AuthRepository>(() => AuthRepository());
  serviceLocator
      .registerFactory<NotificationRepository>(() => NotificationRepository());

  serviceLocator.registerFactory<SplashBloc>(() => SplashBloc());
  serviceLocator.registerFactory<NotificationBloc>(() => NotificationBloc());
  serviceLocator.registerFactory<ChatBloc>(() => ChatBloc());
  serviceLocator.registerFactory<RegisterBloc>(() => RegisterBloc());
  serviceLocator.registerFactory<ConversationBloc>(() => ConversationBloc());
}
