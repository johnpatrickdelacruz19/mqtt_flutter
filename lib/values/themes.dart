import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'colors.dart' as colors;
import 'dimens.dart' as dimens;

/// The themes.dart file is mainly used for determining the overall theme of the app, including the default font to be used

const String kFontFamily = "Prompt";
const double kFontSizeHeadline1 = 40.0;
const double kFontSizeHeadline2 = 22.0;
const double kFontSizeHeadline3 = 18.0;
const double kFontSizeBody2 = 15.0;
const double kFontSizeButtonText = 15.0;

final lightThemeData = ThemeData(
  fontFamily: kFontFamily,
  brightness: Brightness.light,
  primaryColor: colors.primaryColor,
  appBarTheme: const AppBarTheme(
      systemOverlayStyle:
          SystemUiOverlayStyle(statusBarBrightness: Brightness.light),
      elevation: dimens.zero,
      color: colors.transparentColor,
      iconTheme: IconThemeData(
        color: colors.primaryColor, //change your color here
      )),
);
