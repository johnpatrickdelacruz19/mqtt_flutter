/// The strings.dart file contains all the magic strings that are used in the app

const String alertOk = "OK";

const String dialogTitleUhoh = "Uh-oh";
const String dialogTitleOops = "Oops!";
const String dialogTryAgain = "Try again";

const String errorNotRegistered = "This account is not registered";
const String errorInvalidFormat = "Invalid mobile number format";

const String errorMobileExist = "Mobile already exist";

const String errorOTPnotMatch =
    "The OTP you entered is incorrect. Please try again.";

const String errorResend = "Resend is only allowed after ";

const String errorMPinIncorrect = "Your MPIN is incorrect";
const String errorMPinNotMatch = "MPIN does not match";

const String errorIncorrectPin =
    "You entered the incorrect MPIN. Please enter the correct MPIN to avoid 30-minute account lock out.\n\nTo reset your MPIN, tap on “Forgot MPIN”.";

const String errorLockAccount =
    "We’re sorry, it seems that you have reached the maximum number of invalid login attempts.\n\nYour account has been locked, \n please wait ";

const String errorVerfiyLock =
    "You have reached the maximum number of attempts to enter your correct OTP code. You have been locked from using the app, please wait ";

const String errorEmailFormat =
    "Your email is not in the valid list of users. Please contact covid-helpdesk@globe.com.ph to be added to the list.";
const String errorGeneric =
    "An unexpected error occurred. Please try again later.    ";
const String errorNoInternet =
    "You are currently offline. Please switch ON your data or connect to a WiFi hotspot.";

const String applicationNotFound = "Application not found";

const String errorExpiredOTP =
    "Your verification code expired. Please get a new code.";

const String landingLogin = "Log in";
const String landingCreate = "Create an account";

const String loginNext = "Next";

const String successResendTitle = "Resend Successful!";
const String successResendDesc =
    "Your 6-digit verification code has been successfully resent to your mobile number: ";

const String saveProfileSuccess = "Your profile has been successfully updated!";

const String saveProfileSuccessTitle = "Profile Updated!";

const String sendOtpEmailSuccessTitle = "Verification email sent!";

const String deleteProductTitle = "Delete Product";
const String deleteProductBody =
    "Are you sure you want to delete this product?";

const String sendOtpEmailSuccess =
    "A verification email with your 6-digit OTP has been sent to ";

const String verifyEmailOtpSuccessTitle = "Email Verified!";

const String verifyEmailOtpSuccess =
    "Your email address has been \nsuccessfully verified.";

const String storeSaveSuccessTitle = "Store Updated!";

const String storeSaveSuccess =
    "Your store details have been\n successfully added!";

const String storeEditSuccess =
    "Your store details have been\n successfully updated!";

const String storeExist = "Store name already exist";

const String oneGcash = "Store can only have one GCash account";

const String gcashAddedSuccessTitle = "GCash Added!";

const String gcashAddedSuccess =
    "You have successfully added your GCash account as your payment details.";

const String gcashEditSuccessTitle = "GCash Updated!";

const String gcashEditSuccessBody =
    "Your GCash account details have\n been successfully updated!";

const String emailResentSuccessTitle = "Email Resent!";

const String emailResentSuccessBody1 =
    "A verification email with your 6-digit OTP code has been successfully resent to ";
const String emailResentSuccessBody2 = "\n\nKindly check your spam folders too";

const String productAlreadyExist = "Product code already exists";

const String digitCodeError = "Check 6 digit code";

const String skuAlreadyExist = "Sku already exist";

const String storeImageError = "Store image should not be empty";

const String emailAlreadyExist = "Email already exist";

const String expiredTdt = "Trusted device expired";

const String addProductSuccessTitle = "Publish Successful!";

const String addProductSuccessBody =
    "This product has been successfully\n published.";

const String draftProductSuccessTitle = "Draft Saved";

const String draftProductSuccessBody =
    "This product has been successfully\n saved to your drafts.";

const String unpublishProductSuccessTitle = "Unpublish a product";

const String publishProductSuccessTitle = "Publish a product";

const String unpublishProductSuccessBody =
    "Nais mo bang i-unpublish ang product na ito?\n Hindi ito makikita ng buyers sa iyong store kapag ito ay unpublished.";

const String publishProductSuccessBody =
    "Nais mo bang i-publish ang product na ito? Makikita na ito ng buyers sa iyong store kapag ito ay published.";

const String updateProductSuccessTitle = "Success";

const String updateProductSuccessBody =
    "This product has been successfully updated.";

const String updateProductSuccessPublishBody =
    "Your product has been added to your store";
