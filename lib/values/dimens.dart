/// The dimens.dart file holds all the numeric values used in the app

//MARGIN
const double announcementTextButtonSpace = 160;
const double margin = 16;
const double buttonSpace = 42;
const double verySmallSpace = 2.5;
const double bellDot = 4;
const double smallestSpace = 5;
const double xsSpace = 8;
const double dashboardRightMargin = 7.5;
const double smallerSpace = 10;
const double smallSpace = 15;
const double space = 20;
const double mediumSpace = 25;
const double largeSpace = 30;
const double questionAnswerSpace = 45;
const double largerSpace = 40;
const double confirmationNextPreviousSpace = 50;
const double linkMobileVerificationSpace = 55;
const double largestSpace = 60;
const double permissionSpaceConsent = 65;
const double extraLargeSpace = 70;
const double verificationResendSpace = 80;
const double splashSpace = 90;
const double loadingSpace = 100;
const double yesNoNextPreviousSpace = 150;
const double dashboardTopSpace = 130;
const double baseDashboardTopSpace = 200;

//FONTS

const double smallestFontSize = 10;
const double smallerFontSize = 12;
const double smallFontSize = 14;
const double averageFontSize = 15;
const double fontSize = 16;
const double idleSubheaderFontSize = 17;
const double mediumFontSize = 18;
const double qrDateFontSize = 20;
const double titleFontSize = 22;
const double confirmQuestionFontSize = 24;
const double profileNameFontSize = 25;
const double verificationFontSize = 30;
const double largeFontSize = 36;
const double signInSize = 24;

//BORDERS
const double teamBorderWidth = 1;
const double teamBorderRadius = 25;
const double profileBorderWidth = 2;
const double qrBorderWidth = 3;
const double cameraRadius = 150;
const double cameraBottomRadius = 25;
const double borderRadius = 5;
const double borderRadiusSmall = 7.5;
const double borderRadiusMedium = 10;
const double appBarBottomThickness = 8;
const double mobileNumberFieldBorderRadius = 8;
const double mobileNumberFieldBorderWidth = 1;

//SIZE
const double announcementSize = 10;
const double dropdownSize = 10;
const double redDotSize = 10;
const double profilePictureSize = 75;
const double googleIconSize = 28;
const double googleButtonHeight = 40;
const double googleButtonWidth = 200;
const double dividerHeight = 10;
const double mobileNumberDividerThickness = 0.25;
const double checkboxHeight = 15;
const double checkboxHeightColored = 20;
const double checkboxHeightConsent = 25;
const double optionButtonHeight = 48;
const double qrDimension = 250;
const double notificationCloseSpace = 12;
const double notificationCloseSize = 25;
const double mobileButtonWidth = 33;
const double mobileButtonHeight = 55;
const double mobileFieldHeight = 45;
const double mobileFieldWidth = 230;
const double alertHeight1 = 40;
const double alertHeight2 = 35;
const double alertHeight3 = 45;
const double alertFalseOverrideMinWidth = 226;
const double navigationMinWidth = 70;
const double announcementDeletebtn = 80;
const double resendMinWidth = 110;
const double gCheckWidth = 72;
const double cameraButtonSize = 75;
const double traceButtonSize = 75;
const double cameraBottomHeight = 120;
const double loginDividerHeight = 1;
const double loginDividerThickness = 1;
const double unsafeIconHeight = 170;
const double unsafeIconWidth = 250;
const double teamHeight = 35;
const double cardIconSize = 57;
const double bottomLoaderSize = 33;
const double bottomLoaderWidth = 1.5;
const double dashboardBackgroundWidth = 121;
const double dashboardBackgroundHeight = 138;
const double idleAssetWidth = 186;
const double idleAssedHeight = 177;

//SCREEN DIMENSIONS
const double screenHeight = 640;
const double screenWidth = 320;

//OTHERS
const double zero = 0;
const int otpMaxCount = 5;
const double alertElevation = 5;
const double cardElevation = 1;
const double cameraOpacity = 0.75;
const double dashboardOpacity = 0.5;
const double popupOffset = 100;
const int profileLoadDuration = 500;
const int splashDuration = 3;
const int pictureQuality = 600;
const double cameraMaxScale = 3;
const int spacerFlexOne = 1;
const int spacerFlexTwo = 2;
const int maxItemsShownDropdown = 3;
const double blur = 3;
const double twice = 2;
const int announcementCountPage = 10;
const double announcementListScrollRatio = 0.9;

/// TIME
const int thousand = 1000;
const int dayHours = 24;
const int weekDays = 7;
const int dayYears = 365;
const int scrollTransitionDuration = 500;
const int connectionTimeOut = 60000;

// LOADING
const double loadingDimension = 31;
const double loadingStrokeWidth = 4.0;

// CAMERA ICON
const double cameraIcon = 14.0;
const int maxLinesText = 1;

// POSITION
const double redDotRight = 15.0;
const double redDotTop = 18.0;
