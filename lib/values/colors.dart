import 'package:flutter/material.dart';

/// The colors.dart file contains all the color schemes used throughout the app

const Color primaryColor = Color(0xffEE7829);
const Color primaryColor2 = Color(0xffF68E21);
const Color disableColor = Color(0xffFFC180);

const Color textColor = Color(0xff292C2B);
const Color textColor2 = Color(0xff141414);
const Color textDisable = Color(0xff8F8F8F);

const Color checkBoxColor = Color(0xff32BA7C);

const Color termsColor = Color(0xff1777F1);
const Color blueColor = Color(0xff0473E5);

const Color grayColor = Color(0xffE4E4E4);
const Color darkGrayColor = Color(0xffCFDCE6);
const Color switchBgColor = Color(0xffA5AFC0);
const Color lightGray = Color(0xff808080);
const Color grayBg = Color(0xffEEEEEE);
const Color lightGrayBorder = Color(0xffD5D5D5);

const Color darkerBlueColor = Color(0xff023047);

const Color lineColor = Color(0xFFC4C4C4);

const Color appBarBgColor = Color(0xFFFBFBFB);

const Color blackColor = Color(0xff141414);
const Color whiteColor = Colors.white;

const Color redColor = Color(0xffD93025);
const Color redColor2 = Color(0xffD73E3E);

const Color transparentColor = Colors.transparent;

const Color yellowColor = Color(0xffFDB119);

const Color lightGrayColor = Color(0xffB4B4B4);

const Color draftColor = Color(0xff37474F);
