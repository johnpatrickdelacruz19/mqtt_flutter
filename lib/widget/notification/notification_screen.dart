import 'package:cass_rnd/bloc/notification/notification_bloc.dart';
import 'package:cass_rnd/bloc/notification/notification_event.dart';
import 'package:cass_rnd/bloc/notification/notification_state.dart';
import 'package:cass_rnd/bloc/register/register_bloc.dart';
import 'package:cass_rnd/bloc/register/register_state.dart';
import 'package:cass_rnd/locator.dart';
import 'package:cass_rnd/network/services/navigation_services.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class NotificationScreen extends StatefulWidget {
  const NotificationScreen({Key? key}) : super(key: key);

  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  final formKey = GlobalKey<FormState>();
  final _applicationIdController = TextEditingController();
  final _topicController = TextEditingController();

  final NavigationService _navigationService =
      serviceLocator<NavigationService>();

  String status = '';

  @override
  void initState() {
    super.initState();

    _applicationIdController.text = 'cc71f3fb-38f9-4bed-b8e0-972dde191b48';
    _topicController.text = 'test';
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<NotificationBloc, NotificationState>(
      listener: (context, state) {
        if (state is NotificationSubscribeSuccess) {
          if (state.response == 'subscribe') {
            status = 'subscribe';
          } else {
            status = 'unsubscribe';
          }
        }
      },
      child: BlocBuilder<NotificationBloc, NotificationState>(
        builder: (context, state) {
          return Scaffold(
            backgroundColor: Colors.white,
            body: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20, vertical: 50),
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: GestureDetector(
                        onTap: () {
                          _navigationService.goBack();
                        },
                        child: const SizedBox(
                            child: Icon(
                          Icons.arrow_back_ios,
                          color: Colors.black,
                          size: 40,
                        )),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15),
                    child: TextField(
                      controller: _applicationIdController,
                      maxLines: null,
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Application Id',
                          hintText: ''),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 15.0, right: 15.0, top: 15, bottom: 0),
                    child: TextField(
                      controller: _topicController,
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Topic',
                          hintText: ''),
                    ),
                  ),
                  Text('status ' + status),
                  TextButton(
                    onPressed: () {
                      BlocProvider.of<NotificationBloc>(context).add(
                          NotificationSubscribe(
                              applicationId:
                                  _applicationIdController.text.trim(),
                              topic: _topicController.text.trim()));
                    },
                    child: const Text(
                      'Subscribe',
                      style: TextStyle(color: Colors.green, fontSize: 15),
                    ),
                  ),
                  TextButton(
                    onPressed: () {
                      BlocProvider.of<NotificationBloc>(context).add(
                          NotificationUnsubscribe(
                              applicationId:
                                  _applicationIdController.text.trim()));
                    },
                    child: const Text(
                      'Unsubscribe',
                      style: TextStyle(color: Colors.green, fontSize: 15),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
