import 'package:cass_rnd/bloc/conversation/conversation_bloc.dart';
import 'package:cass_rnd/bloc/conversation/conversation_event.dart';
import 'package:cass_rnd/bloc/conversation/conversation_state.dart';
import 'package:cass_rnd/bloc/navigation/navigation_bloc.dart';
import 'package:cass_rnd/bloc/navigation/navigation_event.dart';
import 'package:cass_rnd/model/chat_users.dart';
import 'package:cass_rnd/model/room.dart';
import 'package:cass_rnd/widget/conversation/conversation_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ConversationScreen extends StatefulWidget {
  const ConversationScreen({Key? key}) : super(key: key);

  @override
  _ConversationScreenState createState() => _ConversationScreenState();
}

class _ConversationScreenState extends State<ConversationScreen> {
  List<ChatUsers> chatUsers = [
    ChatUsers(
        name: "Jane Russel",
        messageText: "Awesome Setup",
        imageURL: "https://randomuser.me/api/portraits/men/1.jpg",
        time: "Now"),
    ChatUsers(
        name: "Glady's Murphy",
        messageText: "That's Great",
        imageURL: "https://randomuser.me/api/portraits/men/8.jpg",
        time: "Yesterday"),
    ChatUsers(
        name: "Jorge Henry",
        messageText: "Hey where are you?",
        imageURL: "https://randomuser.me/api/portraits/men/2.jpg",
        time: "31 Mar"),
    ChatUsers(
        name: "Philip Fox",
        messageText: "Busy! Call me in 20 mins",
        imageURL: "https://randomuser.me/api/portraits/men/3.jpg",
        time: "28 Mar"),
    ChatUsers(
        name: "Debra Hawkins",
        messageText: "Thankyou, It's awesome",
        imageURL: "https://randomuser.me/api/portraits/men/4.jpg",
        time: "23 Mar"),
    ChatUsers(
        name: "Jacob Pena",
        messageText: "will update you in evening",
        imageURL: "https://randomuser.me/api/portraits/men/5.jpg",
        time: "17 Mar"),
    ChatUsers(
        name: "Andrey Jones",
        messageText: "Can you please share the file?",
        imageURL: "https://randomuser.me/api/portraits/men/6.jpg",
        time: "24 Feb"),
    ChatUsers(
        name: "John Wick",
        messageText: "How are you?",
        imageURL: "https://randomuser.me/api/portraits/men/7.jpg",
        time: "18 Feb"),
  ];

  final _conversationName = TextEditingController();
  late ConversationBloc _conversationBloc;

  List<Room> roomList = [];

  @override
  void initState() {
    super.initState();

    _conversationBloc = BlocProvider.of<ConversationBloc>(context);

    // Future.delayed(const Duration(seconds: 2), () {
    //   _conversationBloc.add(ConversationListLoad());
    // });
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ConversationBloc, ConversationState>(
      listener: (context, state) {
        if (state is ConversationListSuccess) {
          roomList = state.roomList;
        }
        if (state is ConversationAddSuccess) {
          // try another way
          BlocProvider.of<NavigationBloc>(context)
              .add(NavigationToConversation());
        }
      },
      child: BlocBuilder<ConversationBloc, ConversationState>(
        builder: (context, state) {
          return Scaffold(
            body: SingleChildScrollView(
              physics: const BouncingScrollPhysics(),
              child: SafeArea(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 16, right: 16, top: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          GestureDetector(
                            onTap: () {
                              _conversationBloc.add(ConversationKeySecret());
                            },
                            child: const Text(
                              "Conversations",
                              style: TextStyle(
                                  fontSize: 32, fontWeight: FontWeight.bold),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              _displayTextInputDialog(context);
                            },
                            child: Container(
                              padding: const EdgeInsets.only(
                                  left: 8, right: 8, top: 2, bottom: 2),
                              height: 30,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(30),
                                color: Colors.pink[50],
                              ),
                              child: Row(
                                children: const <Widget>[
                                  Icon(
                                    Icons.add,
                                    color: Colors.pink,
                                    size: 20,
                                  ),
                                  SizedBox(
                                    width: 2,
                                  ),
                                  Text(
                                    "Add New",
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        BlocProvider.of<NavigationBloc>(context)
                            .add(NavigationToNotifications());
                      },
                      child: const Padding(
                        padding: EdgeInsets.only(left: 16, right: 16, top: 10),
                        child: Text(
                          "Notifications",
                          style: TextStyle(
                              fontSize: 32, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 16, left: 16, right: 16),
                      child: TextField(
                        decoration: InputDecoration(
                          hintText: "Search...",
                          hintStyle: TextStyle(color: Colors.grey.shade600),
                          prefixIcon: Icon(
                            Icons.search,
                            color: Colors.grey.shade600,
                            size: 20,
                          ),
                          filled: true,
                          fillColor: Colors.grey.shade100,
                          contentPadding: const EdgeInsets.all(8),
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(20),
                              borderSide:
                                  BorderSide(color: Colors.grey.shade100)),
                        ),
                      ),
                    ),
                    ListView.builder(
                      key: UniqueKey(),
                      itemCount: roomList.length,
                      shrinkWrap: true,
                      padding: const EdgeInsets.only(top: 16),
                      physics: const NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) {
                        return ConversationList(
                          name: roomList[index].name,
                          messageText: roomList[index].messageCount.toString(),
                          imageUrl: chatUsers[index].imageURL,
                          time: chatUsers[index].time,
                          roomId: roomList[index].roomId,
                          isMessageRead:
                              (index == 0 || index == 3) ? true : false,
                        );
                      },
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  Future<void> _displayTextInputDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text(
              "Add new conversations",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            content: TextField(
              controller: _conversationName,
              decoration: const InputDecoration(hintText: "Conversation name"),
            ),
            actions: <Widget>[
              TextButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: const Text('Cancel',
                    style: TextStyle(color: Colors.red, fontSize: 15)),
              ),
              TextButton(
                onPressed: () {
                  // call api room create
                  _conversationBloc.add(ConversationAddName(
                      name: _conversationName.text.toString().trim()));
                  Navigator.pop(context);
                },
                child: const Text('Add',
                    style: TextStyle(color: Colors.green, fontSize: 15)),
              ),
            ],
          );
        });
  }
}
